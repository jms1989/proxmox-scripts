export VM=$(pvesh get /cluster/nextid | grep -oE '[0-9]*')
pct create $VM /var/lib/vz/template/cache/ubuntu-18.04-standard_18.04.1-1_amd64.tar.gz \
-arch amd64 -cores 1 -hostname ct-$RANDOM -memory 512 -net0 name=eth0,bridge=vmbr0,ip=dhcp \
-ostype ubuntu -password "123456789" -storage local-lvm -swap 0 -unprivileged 1 -start 1