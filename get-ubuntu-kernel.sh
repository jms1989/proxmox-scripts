#!/bin/bash
cd /tmp
wget http://mirror.lstn.net/ubuntu/dists/bionic-updates/main/installer-amd64/current/images/netboot/netboot.tar.gz
tar xzf netboot.tar.gz 
mv ubuntu-installer/amd64/linux ubuntu-installer/amd64/initrd.gz .
rm -rf ubuntu-installer netboot.tar.gz ldlinux.c32 pxelinux.0 pxelinux.cfg version.info
exit
