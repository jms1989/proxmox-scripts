export VM=$(pvesh get /cluster/nextid | grep -oE '[0-9]*')
qm create $VM \
--virtio0 local-lvm:8 \
--net0 virtio,bridge=vmbr0 \
--bootdisk virtio0 \
--ostype l26 \
--memory 512 \
--balloon 1024 \
--cores 1 \
--args '-kernel /tmp/linux -initrd /tmp/initrd.gz -append "preseed/url=https://gitlab.com/jms1989/preseed-gitlab-ci-multi-runner/raw/master/gitlab-ci-multi-runner.seed debian-installer/allow_unauthenticated_ssl=true debian/priority=critical vga=normal language=en country=US locale=en_US.UTF-8 keyboard-configuration/xkb-keymap=us console-setup/ask_detect=false netcfg/choose_interface=auto netcfg/get_hostname=PRESEED FRONTEND_BACKGROUND=original"'

qm start $VM #\
#&& ((while (qm status $VM | grep -o running); do sleep 10; done) && qm set $VM -delete args) \
#&& qm start $VM